<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongTableAgin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song', function (Blueprint $table) {
            $table->integer('idSong')->unsigned();
            $table->primary('idSong');
            $table->string('title',255);
            $table->string('link',255);
            $table->string('image',255)->nullable()->default(null);
            $table->string('artist',20);
            $table->integer('priority');
            $table->integer('idAccount')->unsigned();
            $table->float('time');
			$table->boolean('isBlock')->default(false);
            $table->timestamps();
            $table->foreign('idAccount')->references('id')->on('account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('song');
    }
}
