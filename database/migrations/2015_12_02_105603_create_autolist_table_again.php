<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutolistTableAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autolist', function (Blueprint $table) {
            $table->integer('idAuto')->unsigned();
            $table->integer('idSong')->unsigned();
            $table->foreign('idAuto')->references('idAuto')->on('auto')->onDelete('cascade');
            $table->foreign('idSong')->references('idSong')->on('song')->onDelete('cascade');
            $table->primary(['idAuto','idSong']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('autolist');
    }
}
