<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteTableAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote', function (Blueprint $table) {
            $table->integer('idAccount')->unsigned();
            $table->date('date');
            $table->integer('idSong')->unsigned();
            $table->foreign('idAccount')->references('id')->on('account')->onDelete('cascade');
            $table->foreign('idSong')->references('idSong')->on('song')->onDelete('cascade');
            $table->primary(['idAccount','date','idSong']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vote');
    }
}
