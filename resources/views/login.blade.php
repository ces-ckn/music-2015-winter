<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MUSIC 2015 WINTER</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/login-style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- cookie functions -->
	<script>
	function getCookie(c_name){
		var c_value = document.cookie;
		var c_start = c_value.indexOf(" " + c_name + "=");
		if (c_start == -1) {
			c_start = c_value.indexOf(c_name + "=");
		}
		if (c_start == -1) {
			c_value = null;
		} else {
			c_start = c_value.indexOf("=", c_start) + 1;
			var c_end = c_value.indexOf(";", c_start);
			if (c_end == -1) {	
				c_end = c_value.length;
			}
			c_value = unescape(c_value.substring(c_start,c_end));
		}
		return c_value;
	}

	function setCookie(c_name,value,exdays) {
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	}

	function checkCookie() {
		var username=getCookie("username");
		var pass=getCookie("pass");
		if (username!=null && username!="" && pass!=null && pass!="") {
			$('#login-form').find('#input-email').val(username);
			$('#login-form').find('#input-password').val(pass);			
		}
	}
	</script>
</head>

<body onload="checkCookie()">
	<section>
	<article>
		<h1 class="title">MUSIC 2015 WINTER</h1>
        <h3 class="title2">Enjoy your favorite songs!</h3>
	</article>
    <aside>	
		<!-- login form -->
		<form id="login-form" method="POST" action="login">
		
			<!-- username input -->
			<input id="input-email" type="email" name="email"
			data-toggle="popover" data-trigger="manual" data-placement="left" 
			data-content="Invalid email!"
			placeholder="Enter your Email" autofocus ></input><br>
			
			<!-- password input -->
			<input id="input-password" name="password" type="password" 
			data-toggle="popover" data-trigger="manual" data-placement="left" 
			data-content="At least 6 characters!"
			placeholder="Enter your Password" ></input><br>
			
			<!-- confirm password input -->
			<input id="confirm-password-input" name="confirmpassword" type="password" 
			data-toggle="popover" data-trigger="manual" data-placement="left" 
			data-content="This must be same as password!"
			placeholder="Confirm your Password" ></input>
			
			<!-- remember checkbox -->
			<div id="checkbox-form">
				<input type="checkbox" id="cbx-remember" name="cbx-remember" class="css-checkbox" checked="checked"/>
				<label for="cbx-remember" name="checkbox2_lbl" class="css-label lite-orange-check" style="color:white">Remember me!</label>
			</div>
			<br>
			
			<label id="error_messages" style="color: red"></label><br>

			@if (isset($error))
			<script>
				document.getElementById("error_messages").innerHTML="{{ $error }}";
			</script>
			@endif
			<!-- button login & register -->
            <a id="btn-submit" class="btn btn-dark btn-lg">Enjoy</a>
			<br>
			
			<!-- button switch login & register form -->
			<a id="btn-switch">Don't Have an Account?</a>
			<!--abc-->
			
			@if(session('registorError'))
				<script>
					document.getElementById('checkbox-form').style.display='none'
					document.getElementById('confirm-password-input').style.display="inline";
					document.getElementById("login-form").action = "register";
					document.getElementById('btn-switch').innerHTML="Login";
					document.getElementById('btn-submit').innerHTML="Register";
					document.getElementById('error_messages').innerHTML= "{{ session('registorError') }}";
				</script>
				{{ Session::forget('registorError') }}
			@endif
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		</form>		
    </aside>
	</section>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!--Custom js -->
	<script src="js/login-script.js"></script>

</body>
</html>
