<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">
	<link rel="stylesheet" href="css/home-style.css" type="text/css">
	<link rel="stylesheet" href="css/player-style.css" type="text/css">
	<link rel="stylesheet" href="css/change-pass-form-style.css" type="text/css">
	<link rel="stylesheet" href="css/countdown-style.css" type="text/css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>	
	<!-- headerbar-->
	<input id="idAccount" type="hidden" value="{{ session('logined')[1] }}"></input>
	<input id="idAdmin" type="hidden" value="{{ session('logined')[2] }}"></input>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid" style="background-color:rgba(241,89,39,1)">
            <div class="navbar-header" >
                <a class="navbar-brand page-scroll">
					<img style="margin:-15px" src="img/logo.png"></img>
				</a>				
            </div>
			<center>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-top:9px">
              <form class="form-inline">
				<div id="search-form" class="form-group" >
					<span class="glyphicon glyphicon-search" style="color: gray; position: absolute; margin-top: 18px; margin-left: 10px;"></span>	
					<input id="search-input" type="text" class="form-control" placeholder="Search by name or artist">									
				</div>
			</form>
            </div>
			</center>
			<div id="user-group">
				<label id="vote-count">{{ $vote }}</label>
				<a href="#" id="lbl-username" data-toggle="modal" data-target="#changePasswordModal">{{ session('logined')[0] }}</a>
				<a href="logout"><button id="btn-logout" class="btn btn-danger" onClick="logout" >Logout</button></a>
			</div>
        </div>
        <center>
			<div id="formsearch" class="formsearch" >
				<ul id="list-search-dropdown" class="list-group" ></ul>		
			</div>
		<center>
    </nav>
	
	<!-- Change password form -->
	<div class="modal fade" id="changePasswordModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<center class="modal-body">
					<form id="change-pass-form" method="POST" action="changepass">
					<input id="inputPass" type="password" 
					name="pass"
					placeholder="Enter your current password"
					data-toggle="popover" data-trigger="manual" data-placement="left" 
					data-content="Wrong password!"></input><br>
					
					<input id="inputNewPass" type="password" 
					name="newpass"
					placeholder="Enter your new password"
					data-toggle="popover" data-trigger="manual" data-placement="left" 
					data-content="At least 6 characters!"></input><br>
					
					<input id="inputConfirmNewPass" type="password" 
					name="confirmnewpass"
					placeholder="Confirm your new password"
					data-toggle="popover" data-trigger="manual" data-placement="left" 
					data-content="This must be same as password!"></input><br>
					
					<button type="button" id="btnChangePass" class="btn btn-danger">SAVE</button><br>
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					</form>
				</center>
				<div class="modal-footer">
					<form id="goto-admin-form" action="admin">
						<button id="btnAdmin" class="btn btn-danger">GO TO THE ADMIN PAGE!</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!--confirm delete form-->
	<div class="modal fade" id="confirmDeleteModal" role="dialog">
		<div class="modal-dialog modal">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Remove this song?</h4>
				</div>
				<center class="modal-body">
					<h4 id="msgConfirnDelete" class="modal-title"></h4><br>
					<button id="btnConfirmDelete" class="btn btn-danger">OK</button>
				</center>
			</div>
		</div>
	</div>
	
	<section>
		
		<div class="center-form">
		<!--playlist-->
		<div id="playlist-form" class="container playlist-form-to-center">
			<div style="display:block;text-align:center">
			<span id="title-favorite-list" class="title2">FAVORITE LIST</span>
			</div>
			<!--List song-->
			<ul id="list-group" class="list-group">
			<?php 
				$idAccount = session('logined')[1];
			?>
			@if (count($songs)>0)
				@for($i=0,$length=count($songs);$i<$length;$i++)
	                <li id="{{ $songs[$i]->idSong }}" class="list-group-item">
						<label class="label label-danger pull-left" id="priority">{{ $songs[$i]->priority }}</label>
						<label class="songTitleInList" style="margin: 0 110px 0 30px">
							<a onclick="playSongEvent({{ $songs[$i]->idSong }})">{{ $songs[$i]->title }}</a>
						</label>
						@if (!in_array($songs[$i]->idSong,$idSongsVoted))
							@if($i!=0 && $vote!=0)
								<div class="btnThumbsUp btnThumbsUp-hover" onClick="VoteUp({{$songs[$i]->idSong}})"></div>
							@else
								<div class="btnThumbsUp" style="opacity: 0.4;" onClick="VoteUp({{$songs[$i]->idSong}})"></div>
							@endif
							@if($i!=$length-1 && $vote!=0)
								<div class="btnThumbsDown btnThumbsDown-hover" onClick="VoteDown({{$songs[$i]->idSong}})"></div>
							@else
								<div class="btnThumbsDown" style="opacity: 0.4;" onClick="VoteDown({{$songs[$i]->idSong}})"></div>
							@endif
						@else
							<div class="btnThumbsUp" style="opacity: 0.4;" onClick="VoteUp({{$songs[$i]->idSong}})"></div>
							<div class="btnThumbsDown" style="opacity: 0.4;" onClick="VoteDown({{$songs[$i]->idSong}})"></div>
						@endif
						@if($songs[$i]->idAccount==$idAccount || session('logined')[2])
							<div class="btnDelete" onclick="deleteSong({{ $songs[$i]->idSong }});"></div>
						@endif
						
	                </li>
                @endfor
            @else
                <li class="list-group-item" style="color:white" id="nodata" >
					<label class="songTitleInList">No data found.<br><br>Try to add a song.<br>
					Let's search.</label>
				</li>
			@endif
                </ul> <!--end list song-->
			</div>
		<!--results-->
		<div id="result-form" class="container result-form-to-left">
			<div style="display:block;text-align:center">
				<span id="title-results-list">< RESULTS</span>
			</div>
			<ul id="result-list" class="list-group">				
                <li class="list-group-item">
					<div>
					<label class="songTitleInList">No data found.<br><br>Make sure all words are spelled correctly.<br>
					Try different keywords.<br>
					Try more general keywords.</label>
					</div>
                </li>
            </ul> <!--end results-->
		</div>
		
		<!--countdown clock-->
		<div id="countdown-form" class="center-form isHiding">
			<label id="countdown"></label>
			<image id="imgClock" src="img/clock.png"></image>
		</div>
		</div>		
	</section>    
	 <!--player-->
	<footer class="player-form">		
		<!--the player-->
		<div id="player">
			<label id="title-song">Enjoy your favorite music!</label>
			<!--play button-->
			<div style="float:left;position:relative;background-color: rgba(241,89,39,1);width:22%;height:50px;">
				
				<div id="btnNext"></div>				
				<div id="btnPlayer" class="btnPlay"></div>
				<div id="btnPrev"></div>			
			</div>
			<div style="float:right;position:relative;background-color: rgba(241,89,39,1);width:18%;height:50px;">
				<label id="duration">0:00</label>
			</div>
			<!--progress bar-->
			<div id="progress-bar">
				<div id="cursor" style="width:0px;height:40px;margin:5px 0;background-color:rgba(0,0,0,0.7);">
				<!--real player-->
				<iframe id="soundcloud_widget" 
					src="http://w.soundcloud.com/player/?url=&auto_play=true"
					width="120" height="120" frameborder="no" style="margin-bottom: -9999px"></iframe>
				</div>			
			</div>
			
		</div>		
	</footer>

	<!-- Socket.io -->
	<script src="js/socket.io.js"></script>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
	
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

	<!--register API-->
	<script src="http://connect.soundcloud.com/sdk.js"></script>
	<script src="http://w.soundcloud.com/player/api.js"></script> <!--for the player-->
	<script>
		SC.initialize({
			client_id: 'cc83484ad41f1e3dc2f3dc4b2c5df0b2'
		});
	</script>
	
    <!-- Custom Theme JavaScript -->
	<script src="js/util-script.js"></script>
    <script src="js/creative.js"></script>
    <script src="js/socket.io.js"></script>
	<script src="js/home-script.js"></script>
	<script src="js/player-script.js"></script>
	<script src="js/change-pass-form-script.js"></script>
	<script src="js/countdown-script.js"></script>
</body>
</html>
