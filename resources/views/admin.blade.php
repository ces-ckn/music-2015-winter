<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="description" content="">
    <meta name="author" content="">
<title>Admin home page</title>
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="css/admin-style.css" type="text/css">
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">

</head>
<body>
<nav>
<div class="navbar navbar-fixed-top">
<!--define header-->
<div class="container-fluid" style="background-color:rgba(241,89,39,1)">
  <div class="navbar-header">
                <a class="navbar-brand page-scroll" >
					<img style="margin:-15px" src="img/logo.png"></img>
				</a>
</div>
<!--define menu-->
<center>
	<div class="navbar-collapse collapse">
            <ul id="menu" class="nav navbar-nav">
              <li class="active" id="ManageSong"><a href="#" onClick="ManageSong()">ManageSong</a></li>
              <li id="ManageUser" ><a href="#" onClick="ManageUser()">ManageUser</a></li>
              <li id="AutoplaySong"><a href="#" onClick="AutoplaySong()">AutoplaySong</a></li>
              <li id="Control"><a href="#" onClick="Control()">Master/Slave</a></li>
            </ul>
          </div>
		  </center>  
		  <a href="home"></a>
		  <!--define name admin-->
		  <div class="admin">
		  <a href="home" style="color:white">BackHome</a>
		  <a href="logout"><button id="btn-logout" class="btn btn-danger" onClick="logout" >LOGOUT</button></a>
		  </div>
	</div>
	
</nav>
<!--define manage song page-->
<center>
	<div id="SongManagePage" class="SongManagePage">
		<h1>ListSong</h1>
		<div class="PlaylistSong" id="PlaylistSong">
			<ul class="list-group" id="listsong">
			</ul>
		</div>
</div>
<!--define manage user page-->
	<div id="UserManagePage" class="UserManagePage">
		<h1>ListUser</h1>
		<div class="ListUser" id="ListUser">
			<ul class="list-group" id="list-user">
			</ul>
		</div>
	</div>
<!--define autoplay song page-->
	<div id="AutoPlaySongPage" class="AutoPlaySongPage">
		<div class="row">
			<div class="col-xs-6">
				<center>
					<h1>Set Autoplay list</h1>
						<form id="form-autoplay" method="get">
						<!--date auto-->
							<label for="datetime" class="label-form">Date:</label>
							<input type="text" name="date" class="form-control" id="datetimepicker">
							<label id="datetime-error" class="error"></label>
						<!--duration auto-->
							<label for="duration" class="label-form">Duration:</label>
							<input type="number" name="duration" class="form-control" id="setsongortime" placeholder="Numbers of song or time">
							<label id="duration-error" class="error"></label>
						<!--check number or second-->
							<div class="checkbutton">
							<input type="radio" name="number" id="number" value="Number">Number
							<input type="radio" name="number" id="second" value="Second">Second
							<label id="radio-error" class="error"></label>
							<div>
						
						<!--btn submit-->
							<input type="button" value="Add" class="btnAdd" id="btnAdd" onClick="Validate()">
							<label id="add-error" class="error"></label>
					</form>
				</center>
			</div>			
		</div>
<!--define autoplaylist-->
 <div class="col-xs-6" id="Autoplaylist">
	<h1>List autoplay</h1>
		<div class="ListAutoplay" id="ListAutoplay">
			<ul class="list-group" id="autoplaylist">
			</ul>
		</div>
</div>
	</div>
	
<!--define master/slave song page-->
	<div id="ControlPage" class="ControlPage">
		<span>Master/Slave song page</span>
	</div>
</center>

	 <!-- jQuery -->
	 
    <script src="js/jquery.js"></script>
	<script src="js/jquery.datetimepicker.full.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!--realtime-->
    <script src="js/socket.io.js"></script>
	<script src="js/admin-script.js"></script>
</body>
</html>