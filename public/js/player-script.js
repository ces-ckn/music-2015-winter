var playSongEvent;
var isAutoPlay=false;
var isPlaying=false;
$(document).ready(function() {
	var widget = SC.Widget(document.getElementById('soundcloud_widget')), // the real player
		btnPlayer = $('#btnPlayer'),
		currentDuration;
	
	//convert millis to mins
	function millisToMinutesAndSeconds(millis) {
			var minutes = Math.floor(millis / 60000);
			var seconds = ((millis % 60000) / 1000).toFixed(0);
			return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
	}	
	//update progress bar
	function updateProgress(position, duration){
		var progressBarWidth = $('#progress-bar').width(),
			cursorWidth= (progressBarWidth * position) / duration;
		$('#cursor').css("width",cursorWidth);
	} 	
	
	//Action when server play a Song
	socket.on('playSong',function(message){
		if ((message[1]==$('#idAccount').val()) || message[1]=='*'){
			console.log('playSong');
			
			$.get('getSongByID',{IDSong:message[0].idSong},function(result){
				var duration1=  Math.ceil(Number(result.duration)/1000)+1;
				var duration2= Number(message[0].duration);
				if (result!=null){
					//console.log((int)(result.duration-message[0].durtation));
					isAutoPlay=true;
					isPlaying=false;
					playSongEvent(message[0].idSong,(duration1-duration2)*1000,result.titleSong);
				}
			});
		}	
	});

	//Action when finish autoplaylist
	socket.on('finishPlayList',function(){
		console.log('finish playing autoplay');
		alert('Finish playing autoplay');
		isAutoPlay=false;
		isPlaying=false;
	});
	// btnPlayer event
    $('#btnPlayer').click(function() {
    	if (!isAutoPlay)
       		widget.toggle(); // play or pause
       
    });
	
	// change image btnPlayer
	widget.bind(SC.Widget.Events.PLAY, function() {
		btnPlayer.removeClass('btnPlay').addClass('btnPause');
	});			
	widget.bind(SC.Widget.Events.PAUSE, function() {
		btnPlayer.removeClass('btnPause').addClass('btnPlay');
	});
	
	// event play music when click one song
	playSongEvent = function(id,duration,titleSong){
		if ((isAutoPlay && !isPlaying)||(!isAutoPlay)){
		isPlaying=true;
		// change blackground
		$('li').css("background-color","rgba(0,0,0,0.7)");
		
		//get title
		var title = $('#'+id).find('.songTitleInList').text(),
			titleLength = title.length,
			spaces = " \u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0 ";
		if (duration!=null){
			title=titleSong;
		}
		if(titleLength <= 30){
			title=title+spaces+spaces+title+spaces+spaces+title;
		}else if(titleLength <= 60){
			title=title+spaces+spaces+title;
		}
				
		//reload widget
		widget.load("https://api.soundcloud.com/tracks/"+id,{
			auto_play:true
		});		
	
		// when the player is loaded already
		widget.bind(SC.Widget.Events.READY, function() {
			//show title
			$('#title-song').text(title);			
			//hightlight song
			//$('[id^='+id+']').css("background-color","rgba(0,0,0,1)"); //must be id^= to find all the same ids	
			$('.songTitleInList a').css("color","white");
			$('[id^='+id+']').find('.songTitleInList a').css("color","red"); //must be id^= to find all the same ids	
			
			// get duration of song
			widget.getDuration(function(duration2){
				currentDuration = duration2;
			});

			widget.bind(SC.Widget.Events.PLAY, function() {
				if (duration>0){
					console.log('duration'+ duration);
					widget.seekTo(duration);
					duration=0;
				}
			});	

		
			// update progessbar and duration on screen
			widget.bind(SC.Widget.Events.PLAY_PROGRESS, function() {
				widget.getPosition(function(position){
					$('#duration').text(millisToMinutesAndSeconds(position));
					updateProgress(position,currentDuration);
				});
				


				// jump to a duration when click on progress bar
				$('#progress-bar').click(function(e){
					if(!isAutoPlay){
						var progressBar = $(this),
							xPos = e.pageX - progressBar.offset().left, // get position
							progressBarWidth = progressBar.width();
						$('#cursor').css("width",xPos); // set width for the cursor
						widget.seekTo((xPos * currentDuration) / progressBarWidth); // jump to the duration
					}
				});
			});			
			
			
			var songPlaying = $('#list-group > #'+id); // the current or the last <li> tag
			// play next song in the playlist when a song finish playing
			widget.bind(SC.Widget.Events.FINISH, function() {
				if (!isAutoPlay){
					var isInPlaylist = songPlaying.length>0,
					nextSong = songPlaying.next(),
					isExistNextSong = nextSong.length>0,
					idNextSong = nextSong.attr('id');
					// if the song has played is existing in the playlist
					if(isInPlaylist && isExistNextSong){
						playSongEvent(idNextSong);
					}
				}
			});
			
			//set event for btnNext
			$('#btnNext').click(function(){
				if (!isAutoPlay){
					var isInPlaylist = songPlaying.length>0,
					nextSong = songPlaying.next(), //search every clicks to make sure still have next song
					isExistNextSong = nextSong.length>0,
					idNextSong = nextSong.attr('id');
					// if the song has played is existing in the playlist
					if(isInPlaylist && isExistNextSong){
						playSongEvent(idNextSong);
					}
				}
			});
			
			//set event for btnPrev
			$('#btnPrev').click(function(){
				if (!isAutoPlay){
					var isInPlaylist = songPlaying.length>0,
					prevSong = songPlaying.prev(),
					isExistPrevSong = prevSong.length>0,
					idPrevSong = prevSong.attr('id');
					// if the song has played is existing in the playlist
					if(isInPlaylist && isExistPrevSong){
						playSongEvent(idPrevSong);
					}
				}
			});
		});	
		}
	};
});