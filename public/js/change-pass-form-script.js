$(document).ready(function(){
	var passInput = $('#inputPass'),
		newPassInput = $('#inputNewPass'),
		confirmNewPassInput = $('#inputConfirmNewPass');
	
	// hide btnAdmin if it's not the admin
	if($('#idAdmin').val()==0){
		$('.modal-footer').hide();
	}
	
	// check valid password
	function isPassword(pass) {
		var regex = /^\S{6,}$/;
		return regex.test(pass);
	}
	
	// btnChangePass event
	$('#btnChangePass').click(function(){		
		var pass = passInput.val(),
			newPass = newPassInput.val(),
			confirmNewPass = confirmNewPassInput.val(),			
			_token = $('input[name="_token"]').val();
		
		// check if the current password is right
		$.post('checkpass',{ _token:_token, pass:pass},function(result){
			if(result==="false"){
				passInput.popover("show");
			} else {
				// if it's right, hide popover
				passInput.popover("hide");				
		
				// if it's right, change password
				if(isPassword(newPass) && newPass===confirmNewPass){
					$.post('changepass',{ _token:_token, newPass:newPass},function(result){
						alert(result);
						$('#changePasswordModal').modal('hide');
					},'text');
				}
			}
		},'text');
		
		if(!isPassword(newPass)){
			newPassInput.popover("show");
		} else {
			newPassInput.popover("hide");
		}
		
		if(newPass!==confirmNewPass){
			confirmNewPassInput.popover("show");
		} else {
			confirmNewPassInput.popover("hide");
		}
	});
	
	// clear texts when close the form
	$('#changePasswordModal').on('hidden.bs.modal', function () {
		newPassInput.val("");
		confirmNewPassInput.val("");
		passInput.val("");
	});
	
	//btnAdmin event
	$('#btnAdmin').click(function(){
		$('#goto-admin-form').submit(); //jump to the admin page
	});
});