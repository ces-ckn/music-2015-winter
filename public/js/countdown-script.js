$(document).ready(function() {
	var countDown = $('#countdown');	
	function formatSeconds(seconds) {
    	var date = new Date(1970,0,1);
    	date.setSeconds(seconds);
    	return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
	}	
	$("#countdown-form").click(function() {
		if($(this).hasClass('isHiding')){
			$(this).animate({left: "0px"});
			$(this).removeClass('isHiding');
		} else {
			$(this).animate({left: "-265px"});
			$(this).addClass('isHiding');
		}
	});
	countDown.text("00:00:00");
	socket.on('countdown',function(data){
		countDown.text("");
		countDown.text(formatSeconds(data));
	});
});