var socket = io.connect('localhost:5000');

var allTracks={
	addTrack: function (name, idSong, titleSong, artist, linkSong, email, duration, imageLink){
		this[name]={idSong:idSong,
		titleSong:titleSong,
		artist:artist,
		linkSong:linkSong,
		email:email,
		duration:duration,
		imageLink:imageLink};
	}
};
//

var addSong,VoteUp,VoteDown,deleteSong,isSearching=false,search;
var handler_Logic;
$(document).ready(function() {
	//define addSonf function
	socket.on('connect',function(){
		console.log('home-connect '+$('#idAccount').val());
		socket.emit('newConnect',$('#idAccount').val())
	});

	addSong =function(indexTrack){	
		var idAccount = $("#idAccount").val();
		var track = allTracks["track"+indexTrack];
		var jsonTrack = JSON.stringify(track);
		$.get('addSong',{clickedTrack: jsonTrack},function(result){
			//socket.emit('addSong',result);			
		});
	};

	socket.on('addSongToPlaylistClient',function (result){
		var idAccount = $("#idAccount").val();
		var idAdmin=$('#idAdmin').val();
		console.log(idAdmin);	
		var startLi='<li id="'+result.idSong+'" class="list-group-item">',
				priority='<label class="label label-danger pull-left" id="priority">'+result.priority+'</label>',
				titlesong=	'<label class="songTitleInList" style="margin-right:120px;">'+
							'<a onclick="playSongEvent('+result.idSong+')">'+result.titleSong+'</a>'+
							'</label>',
				votebutton='<div class="btnThumbsUp btnThumbsUp-hover" onClick="VoteUp('+result.idSong+')"></div>'+
					'<div class="btnThumbsDown btnThumbsDown-hover" onClick="VoteDown('+result.idSong+')"></div>',
				deletebutton='<div class="btnDelete" onclick="deleteSong('+result.idSong+');" ></div>',
				endLi='</li>';
				if(result.idAccount==idAccount||idAdmin==1){
					$('#list-group').append(startLi+priority+titlesong+votebutton+deletebutton+endLi);
					}
				else{
					$('#list-group').append(startLi+priority+titlesong+votebutton+endLi);
					}
				$('#nodata').remove();
				//foreach (findElementsWithId('#'+track.idSong) as idTrackLine)
				$.each(findElementsWithId('#'+result.idSong), function( index, idTrackLine ) {
					//remove btn Add when adding is successful
					idTrackLine.find('.btnAddSongToList').hide();
					//show vote buttons
					idTrackLine.append(
						'<div class="btnThumbsUp btnThumbsUp-hover" onclick="VoteUp('+result.idSong+')"></div>'+
						'<div class="btnThumbsDown btnThumbsDown-hover" onclick="VoteDown('+result.idSong+')"></div>'+
						'<div class="btnDelete" onclick="deleteSong('+result.idSong+');"></div>');
					idTrackLine.find('.songTitleInList').attr('style','margin-right:120px;');
					//update priority
					idTrackLine.find('#priority').text(result.priority);
				});
			
				handler_Logic();
				
				if($('#playlist-form').hasClass("playlist-form-to-right")){
					$('#playlist-form [id^='+result.idSong+']').find('#priority').css("margin-right", "0px");
				}else{
					$('#playlist-form [id^='+result.idSong+']').find('#priority').css("margin-right", "85px");
				}
		//	}
		});
		
	

	//define voteup function
	 VoteUp=function(idSong){
		var idAccount = $("#idAccount").val();
		var priority=$('#playlist-form #list-group #'+idSong+' #priority').text();
		//console.log(idAccount);
		//console.log(priority);
		if (parseInt($('#vote-count').text())==0)
			return;
		//ajax call Homecontroller
		$.get('home/voteUp',
			{
				IDSong:idSong,
				IDAccount:idAccount,
				Priority:priority
			},
			function(result)
			{
				if (result[1]){
					update_vote(result[0]);
					hideVoteButton(idSong);
					var list_group=$('#list-group');
					//changeElement(list_group.find('#'+idSong).prev(),list_group.find('#'+idSong));
					var message=[list_group.find('#'+idSong).prev().attr('id'),idSong+""];
					console.log('asdasdasd');
					socket.emit('voteSong',message);
				}
			});
		
	};
	//define votedown function
	VoteDown=function(idSong){
		var idAccount = $("#idAccount").val();
		var priority=$('#playlist-form #list-group #'+idSong+' #priority').text();
		if (parseInt($('#vote-count').text())==0)
			return;
		//ajax call Homecontroller
		$.get('home/voteDown',
			{
				IDSong:idSong,
				IDAccount:idAccount,
				Priority:priority
			},
			function(result)
			{
				if (result[1]){
					update_vote(result[0]);
					var list_group=$('#list-group');
					hideVoteButton(idSong);
					//changeElement(list_group.find('#'+idSong),list_group.find('#'+idSong).next());
					//hideVoteButton(idSong);
					var message=[idSong+"",list_group.find('#'+idSong).next().attr('id')];
					socket.emit('voteSong',message);
				}
			});

	};
	
	var showResult = function(){
		//get Id from data
		$.get('getAllIdSong',{},function(result){
			var textSearch = $('#search-input').val();

			if(textSearch!=null && textSearch!=='' && textSearch.replace(/\s/g, '')!==''){
				//to right playlist
				$('#playlist-form').removeClass("playlist-form-to-center").addClass("playlist-form-to-right");
				// fix margin
				$('#list-group').find('[id^=priority]').css("margin-right", "0px");
				
				//show results list
				$('#result-form').fadeIn(1000);
				//remove all li tags
				//$('#result-list li').remove();
				//search from API
				//searchFromAPI(textSearch,result);
			}
		});
	};
	
	//show Result dropdown
		var showResultDropdown=function(textSearch){
		$.get('getAllIdSong',{},function(result){
			if(textSearch!=null && textSearch!=='' && textSearch.replace(/\s/g, '')!==''){
				if(!$('#playlist-form').hasClass('playlist-form-to-right')) {
					$('#formsearch').slideDown();
				}
				$('#list-search-dropdown li').remove();
				$('#result-list li').remove();
				//search from API
				searchFromAPI(textSearch,result);
			} else {
				$('#formsearch').slideUp();	
			}
		});
	};

	var searchFromAPI = function(text, result) {
	    search=text;
	    if (!isSearching){
	    	//search when we are searching
		    do{
		    	isSearching=true;
			    handler_search(search,result);
			}while(search.length!=0);
		}

	};
	var handler_search= function(textSearch,result){
		search='';
		SC.get('/tracks', {
		        q: textSearch,
		        limit: 100
		    }, function(tracks) {
		        $(tracks).each(function(index, track) {
		            var list_dropdown = $('#list-search-dropdown');
		            var resultList = $('#result-list');
		            var indexTrack = index + 1;
		            allTracks.addTrack(
		                "track" + indexTrack,
		                track.id,
		                track.title,
		                track.title,
		                track.uri,
		                $('#lbl-username').val(),
		                track.duration,
		                track.artwork_url);
		            var startHtml = '<li id="' + track.id + '" class="list-group-item">',
		                indexHtml = '<label class="label label-danger pull-left" id="priority">' + indexTrack + '</label>',
						indexHtml2 = '<label class="label label-danger pull-left" id="priority">-</label>',
		                titleHtml = '<label class="songTitleInList">' +
		                '<a onclick="playSongEvent(' + track.id + ')">' + track.title + '</a>' + '</label>',
		                endHtml = '</li>',
		                addSongHtml = '<div class="btnAddSongToList" onclick="addSong(' + indexTrack + ')"></div>',
		                voteBtnsHtml = '<div class="btnThumbsUp btnThumbsUp-hover" onclick="VoteUp(' + track.id + ')"></div>' +
		                '<div class="btnThumbsDown btnThumbsDown-hover" onclick="VoteDown(' + track.id + ')"></div>',
		                addSongNotShowHtml = '<div class="btnAddSongToList" onclick="addSong(' + indexTrack + ')" style="display:none"></div>';
		            //if ($('#list-group').find('#'+track.id).find('.btnThumbsUp'))
		            if ($('#list-group').find('#'+track.id).length==1){
		            	resultList.append(startHtml+$('#list-group').find('#'+track.id).html()+endHtml);
		            	list_dropdown.append(startHtml+$('#list-group').find('#'+track.id).html()+endHtml);
		            }else{
			            if (result.IdSongsOfAccount.indexOf(track.id) != -1)
			                voteBtnsHtml += '<div class="btnDelete" onclick="deleteSong(' + track.id + ');"></div>';
			            if (result.IdSongs.indexOf(track.id) == -1) {
			                //console.log(result.indexOf(track.id)+" "+track.title);}
			                resultList.append(startHtml + indexHtml2+titleHtml + addSongHtml + endHtml);
			                list_dropdown.append(startHtml + indexHtml2+ titleHtml + addSongHtml + endHtml);
			            } else {
			                resultList.append(startHtml + indexHtml + titleHtml + addSongNotShowHtml + voteBtnsHtml + endHtml);
			                ;
			                list_dropdown.append(startHtml + indexHtml + titleHtml + addSongNotShowHtml + voteBtnsHtml + endHtml);
			                
			            }
						// change margin right title when showing 3 buttons
			            resultList.find('#' + track.id + ' .songTitleInList').attr('style', 'margin-right:120px;')
						list_dropdown.find('#' + track.id + ' .songTitleInList').attr('style', 'margin-right:120px;');
		        	}
		        });
				if(!$('#playlist-form').hasClass('playlist-form-to-right')){
					$('#formsearch').slideDown();
				}
		        isSearching = false;
		        handler_Logic();
		    });
	};	

	var animation = function (){
		var playlist = $('#playlist-form');
		//to right playlist
		if(playlist.hasClass("playlist-form-to-right")){
			playlist.removeClass("playlist-form-to-right").addClass("playlist-form-to-center");
			// fix margin
			$('#list-group').find('[id^=priority]').css("margin-right", "85px");
		}else{
			playlist.removeClass("playlist-form-to-center").addClass("playlist-form-to-right");
			// fix margin
			$('#list-group').find('[id^=priority]').css("margin-right", "0px");
		}
		//show results list
		$('#result-form').fadeToggle(1000);
	};
	
	//onclick title list
	$('#title-favorite-list').on('click', animation); 
	$('#title-results-list').on('click', animation);

	//dropdown search
	$('#search-input').keyup(function(e){		
		if(e.which==13){
			$('#formsearch').slideUp();
			var textSearch = $(this).closest('#search-form').find('#search-input').val();
			if(textSearch!=null && textSearch!==''){
				//$('#formsearch').hide(1000);
				showResult();	
			}
		} else {
			e.preventDefault();		
			var textSearch = $(this).closest('#search-form').find('#search-input').val();
			showResultDropdown(textSearch);
			//$('#result-form').hide();
			//$('#playlist-form').removeClass("playlist-form-to-right").addClass("playlist-form-to-center");
		}
	});
	
	// disable default event refresh page of enter key
	$('#search-input').keydown(function(e){
		if(e.which==13){
			e.preventDefault();
		}
	});
	
	//hide formsearch when click document
	$(document).click(function() {
		$('#formsearch').slideUp();
		$('#search-input').val("");
	});	
	// don't hide formsearch when click formsearch
	$('#formsearch').click(function(e){
		e.stopPropagation();
	});
	//$('#formsearch').onmoveleave(function(e){
		//$('#search-input').val("");
	//});
	
	//event when we click button delete the song
	deleteSong = function(id){
		var confirmDeleteModal = $("#confirmDeleteModal"),
			btnConfirmDelete = $("#btnConfirmDelete"),
			songTitle = $('#'+id+' .songTitleInList').text();
		
			confirmDeleteModal.find('#msgConfirnDelete').text('Remove: '+songTitle+'?');
			confirmDeleteModal.modal('show');	
			btnConfirmDelete.off("click"); //remove all events have setted before
		btnConfirmDelete.click(function(e){
			$.get('home/deleteASong',{idSong:id},function(result){
			var message=[id,result];
			socket.emit('deleteSong',message);
			confirmDeleteModal.modal('hide');	
			},'text');
		});
	};
	
	findElementsWithId = function(id){
		var result_list= $('#result-list');
		var list_search_dropdown= $('#list-search-dropdown');
		return [result_list.find(id),list_search_dropdown.find(id)];
	};

	var update_vote= function(voted){
		$('#vote-count').text(5-voted);
	};

	var changeElement= function(element1,element2){		
		var temp= element1.html();
		var id= element1.attr('id');		
		/*
		var check=true;
		var idd=element1.attr('id');
		if ($('#'+idd).find('#priority').text()==1 && !($('#'+idd).find('.btnThumbsDown').hasClass('btnThumbsDown-hover'))){
			check=false;
		}
		
		var iddd=element2.attr('id');
		if (element2.next().length==0 && !($('#'+iddd).find('.btnThumbsUp').hasClass('btnThumbsUp-hover'))){
			check=false;
			idd=iddd;
		}
		*/
		var check=false;
		element1.fadeOut(500, function(){
			element1.html(element2.html());
			element1.attr('id',element2.attr('id'));
			element2.html(temp);
			element2.attr('id',id);			
			var t= element1.find('#priority').text();
			element1.find('#priority').text(element2.find('#priority').text());
			element2.find('#priority').text(t);	
			
			if (!check){
				handler_Logic();
				check=true;
			}
			
			element1.fadeIn(500);
		});
		element2.fadeOut(500, function(){			
			element2.fadeIn(500);
			if (!check){
				handler_Logic();
				check=true;
			}
		});
		
	};

	handler_Logic= function(){
		if ($('#vote-count').text()=='0'){
			//hide every elements when vote = 0
			$('#list-group').find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
			$('#list-group').find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
			$('#result-list').find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
			$('#result-list').find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
			$('#list-search-dropdown').find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
			$('#list-search-dropdown').find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
		}else{
			
			var list_group=$('#list-group li');
			
			//get the first track in favorite list
			var first_track=list_group.first();
			var elements= findElementsWithId('#'+first_track.attr('id'));

			//add all track are the same id
			var allTracksWithAId=[first_track,elements[0],elements[1]];
			$.each(allTracksWithAId,function(index, track) {
				//hide up button of the first element in favorite list
				track.find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
			});

			elements= findElementsWithId('#'+first_track.next().attr('id'));
			var allTracksWithAId=[first_track.next(),elements[0],elements[1]];
			$.each(allTracksWithAId,function(index, track) {
				//show up button of the button second in in favorite list
				track.find('.btnThumbsUp').fadeTo(0,1).addClass('btnThumbsUp-hover');
			});

			//get the last track in favorite list
			var last_track= list_group.last();
			elements= findElementsWithId('#'+last_track.attr('id'));

			//add all track are the same id
			allTracksWithAId=[last_track,elements[0],elements[1]];
			$.each(allTracksWithAId,function(index, track) {
				//hide down button of the last element in list_group
				console.log(track.attr('id'));
				track.find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
			});

			elements= findElementsWithId('#'+last_track.prev().attr('id'));
			allTracksWithAId=[last_track.prev(),elements[0],elements[1]];
			$.each(allTracksWithAId,function(index, track) {
				//show down button of the element before last in list_group
				track.find('.btnThumbsDown').fadeTo(0,1).addClass('btnThumbsDown-hover');
			});

			$.get('getListIdSongVoted',{idAccount:$('#idAccount').val()},function(result){
				//var list = $('#list-group');
				/*for (var i in result){
					var track=list.find('#'+i);
					var elements1= findElementsWithId('#'+i);
					var allTracksWithAId1=[track,elements1[0],elements1[1]];
					$.each(allTracksWithAId1,function(index, track) {
						track.find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
						track.find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
					});
					console.log(i);
					hideVoteButton(i);
				}*/
				$.each(result,function(index,data){
					hideVoteButton(data);
				});
			});
		}
	};

	var hideVoteButton= function(idsong){
		var track= $('#list-group').find('#'+idsong);
		var elements= findElementsWithId('#'+idsong);
		var allTracksWithAId= [track,elements[0],elements[1]];
		$.each(allTracksWithAId, function(index, el) {
			el.find('.btnThumbsUp').fadeTo(0,0.4).removeClass('btnThumbsUp-hover');
			el.find('.btnThumbsDown').fadeTo(0,0.4).removeClass('btnThumbsDown-hover');
		});
	};

	//Realtime
	//DeleteSong
	socket.on('deleteSong',function(message){
		var id=message[0];
		var result=message[1];
		var list_group = $('#list-group');
		var item = list_group.find('#'+id);
		if (item != null){
			var next_item=item.next();
			while (next_item.length){
				var ids=findElementsWithId('#'+next_item.attr('id'));
				var items=[next_item,ids[0],ids[1]];
				var priority=result;
				$.each(items, function( index, value ) {
					value.find('.label').text((priority) +"");
				});
				result++;
				next_item=next_item.next();
			}
			item.fadeOut(500, function(){					
				item.remove();
				handler_Logic();
			});
		}
		$.each(findElementsWithId('#'+id), function( index, value ) {
			//remove buttons: delete, up, down 
			value.find('.btnThumbsUp').remove();
			value.find('.btnThumbsDown').remove();
			value.find('.btnDelete').remove();
			//show Add Song button
		value.find('.btnAddSongToList').show();
				//remove priority text
		value.find('#priority').text('-');				
		});
		
	});
	
	socket.on('voteSong',function(message){
		var list_group=$('#list-group');
		changeElement(list_group.find('#'+message[0]),list_group.find('#'+message[1]));
		//handler_Logic();
	});

	socket.on('blockSongClient',function (data){
		var list_group = $('#list-group');
		var result_list = $('#result-list');
		var item = list_group.find('#'+data.idSong);
		if (item != null){
			var next_item=item.next();
			while (next_item.length){
				var ids=findElementsWithId('#'+next_item.attr('id'));
				var items=[next_item,ids[0],ids[1]];
				var priority=data.priority;
				$.each(items, function( index, value ) {
					value.find('.label').text((priority) +"");
				});
				data.priority++;
				next_item=next_item.next();
			}
			item.fadeOut(500, function(){					
				item.remove();
			});
		}
		$.each(findElementsWithId('#'+data.idSong), function( index, value ) {
			//remove buttons: delete, up, down 
			value.find('.btnThumbsUp').remove();
			value.find('.btnThumbsDown').remove();
			value.find('.btnDelete').remove();
			//show Add Song button
			value.find('.btnAddSongToList').show();
			//remove priority text
			value.find('#priority').text('-');				
		});			
		handler_Logic();

	});
	
	socket.on('playSong',function(message){
		if ((message[1]==$('#idAccount').val()) || message[1]=='*'){
			console.log('playSong1');
			$.get('home/deleteASong',{idSong:message[0].idSong},function(result){
			},'text');
		}
	});
	socket.on('finishPlayList',function(data){
		console.log("finish1");
	});
	
	
});