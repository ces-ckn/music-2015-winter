var socket = io.connect('localhost:5000');
socket.on('connect',function(){
	console.log('admin-connect');
});

//Define javascript for admin home page
var ManageSong,ManageUser,AutoplaySong,Control,BlockSong,UnBlockSong,BlockUser,UnBlockUser,Validate,DeleteAutoplay;

$(document).ready(function(){
	$('#datetimepicker').datetimepicker();
	//define variable to count number page
	var count=0;
	//define show all song in playlist
	var showSongs=function(){
		$('#listsong li').remove();
	//get result from database
		$.get('getAllSongAdmin',{},function(result){
	//append interface for every value
		$.each(result, function(index,value){
			var title='<li id="'+value.idSong+'" class="list-group-item list-group-item-success">'+value.title,
				priority='<label class="label label-danger pull-left" id="priority">-</label>',
				block='<span class="block" id="SongBlock">'+
					'<img class="btnBlocks" id="blocksong" src="img/block.png" onClick="BlockSong('+value.idSong+')"></img>'+
					'</span>'+	
					'</li>',
				unblock='<span class="block" id="SongBlock">'+
					'<img class="btnBlocks" id="unblocksong" src="img/unblock.png" onClick="UnBlockSong('+value.idSong+')"></img>'+
					'</span>'+	
					'</li>';
				if(value.isBlock==false){
						$('#listsong').append(title+priority+block);
				}
				else{
					$('#listsong').append(title+priority+unblock);
				}
	
			});
		});
	};

	//define show all users
	var showUsers=function(){
			$('#list-user li').remove();
			$.get('getAllUserAdmin',{},function(result){
			console.log(result);
			$.each(result, function(index,value){
				var email='<li id="'+value.id+'" class="list-group-item list-group-item-success">'+value.email,
					priority='<label class="label label-danger pull-left" id="priority">-</label>',
					block='<span class="block" id="UserBlock">'+
					'<img class="btnBlocks" id="blockuser" src="img/block.png" onClick="BlockUser('+value.id+')"></img>'+
					'</span>'+
					'</li>';
					unblock='<span class="block" id="UserBlock">'+
					'<img class="btnBlocks" id="unblockuser" src="img/unblock.png" onClick="UnBlockUser('+value.id+')"></img>'+
					'</span>'+	
					'</li>';
					if(value.isBlock==false){
							$('#list-user').append(email+priority+block);
					}
					else{
						$('#list-user').append(email+priority+unblock);
					}
			});
		});
	};

	//define show list autoplay
	var showListAutoplay=function(){


		$('#autoplaylist li').remove();

		$.get('GetAutoList',{},function(result){
			console.log(result);
			$.each(result,function(index,value){
			$('#autoplaylist').append(
			'<li class="list-group-item" id="'+value.idAuto+'">'+value.startTime+
			'<label class="label label-danger pull-left" id="sort">-</label>'+
			'<button class="btn btn-danger pull-right" id="delete" onclick="DeleteAutoplay('+value.idAuto+')">Delete</button>');	
			$('#datetimepicker').val('');
			$('#setsongortime').val('');
			});
		});
	};
	
	//validate form add autoplay
	Validate=function(){
		var date=$('#datetimepicker').val(),
		duration=$('#setsongortime').val();
			console.log(date);
			var datecurrent=new Date(),
			datetime=new Date(date);
			var count=(datetime.getTime()-datecurrent.getTime());
			count=Math.ceil(count/1000);
			if(checkDateTime(date)&&checkRadio(duration)==0){
				console.log('You choosed number');
				$('#add-error').text('');
				$('#btnAdd').removeClass('afterAdd');
				$.get('Admin/addAutoplayPoint',{
					timeStart:date,
					number:duration,
					duration:0
				},
				function(result){
					console.log('secondLeft'+result.secondLeft);
					if(result){
						//console.log(result);
						showListAutoplay();
						///
						var message=[result.idAuto,count,false,duration];
						socket.emit('setAuto',message);
					}

				});				
			}
			else if(checkDateTime(date)&&checkRadio(duration)==1){
				console.log('You choosed second');
				$('#add-error').text('');
				$('#btnAdd').removeClass('afterAdd');
				$.get('Admin/addAutoplayPoint',{
					timeStart:date,
					number:0,
					duration:duration
				},
				function(result){
					//console.log('secondLeft'+result.secondLeft);
					if(result){
						
						showListAutoplay();
						///
						var message=[result.idAuto,count,true,duration];
						socket.emit('setAuto',message);
					}
				});					
			}
			else{
				$('#add-error').text('Can not insert');
				$('#btnAdd').addClass('afterAdd');
			}
	};
	//define check radio button
	var checkRadio=function(duration){
		if($('#number').is(':checked')){
			console.log('number is checked');
			$('#radio-error').text('');
			$('#duration-error').text('');
			if(checkNumber(duration)){
				return 0;
			}	
		}
		else if($('#second').is(':checked')){
			console.log('second is checked');
			$('#radio-error').text('');
			$('#duration-error').text('');
			if(checkSecond(duration)){
				return 1;
			}
		}
		else{
			$('#radio-error').text('Please choose number or second');
			$('#radio-error').addClass('checkbutton');
			return -1;
		}
	};
	
	//define checkNumberOfSong
	var checkNumber=function(duration){
		//compare duration and numberofsongs
		if(duration==""||duration==null){
			$('#duration-error').text('Please provide number of songs');
			return false;
		}
		else if(duration<0){
			$('#duration-error').text('Number of songs must bigger than 0');
			return false;
		}
		else
			return true;
	};
	//define checksecondOfSong
	var checkSecond=function(duration){
		//compare duration and second of songs
		if(duration==""||duration==null){
				$('#duration-error').text('Please provide time of songs');
				return false;
			}
			else if(duration<0){
				$('#duration-error').text('Second of songs must bigger than 0');
				return false;
			}
			else
				return true;
	};
	//define check date time
	var checkDateTime=function(date){
		var datecurrent=new Date(),
		datetime=new Date(date);
		var count=(datetime.getTime()-datecurrent.getTime());
		console.log('jaja'+count);
		//console.log('curent:'+datecurrent)
		var errordatenull="Please provide date to open the autoplay",
			erroroverdate="DateTime must to over current date";
			if(date==""|| date==null){
				$('#datetime-error').text(errordatenull);
				return false;
			}
			if(datetime < datecurrent){
				$('#datetime-error').text(erroroverdate);
				return false;
			}
			if (datetime > datecurrent){
				$('#datetime-error').text('');
				return true;
			}
	};
	//define delete autoplay
	DeleteAutoplay=function(id){
		$.get('Admin/DeleteAutoplayPoint',
		{
			idAuto:id
		},
		function(result){
			console.log(result);
			if(result){
				$('#autoplaylist #'+id).remove();
				showListAutoplay();
				socket.emit('deleteAutoPlayList',result);
			}
		});
	};
	//define block song
	
	BlockSong= function(idSong)
	{
		//block song in database
		$.get('Admin/BlockSong',{
			IDSong:idSong
		},
		function(result){
			if(result.isBlock==true){
				//find btnBlock to hide
					
			}
		});
	};
	//realtime for block a song
	socket.on('blockSongClient',function (data){
		console.log('block'+data.idSong);
		$('#listsong #'+data.idSong+' #SongBlock #blocksong').hide();
		//replace btnBlock to btnUnblock
		$('#listsong #'+data.idSong+' #SongBlock').append(
		'<img class="btnBlocks" id="unblocksong" src="img/unblock.png" onClick="UnBlockSong('+data.idSong+')"></img>');
	});

	//define unblock song
	
	UnBlockSong=function(idSong)
	{
		//unblock song in database
		
		$.get('Admin/UnBlockSong',{
			IDSong:idSong
		},
		function(result){
			if(result.isUnBlock==true){
			//remove song to the playlist
			$('#listsong #'+idSong).remove();
			
			}
		});
		
	};
	
	//define block user
	
	BlockUser=function(idUser){
		//block user in database
		
		$.get('Admin/BlockUser',{
			idUser:idUser
		},
		function(result){
			if(result.isBlock==true){
				$('#list-user #'+idUser+' #UserBlock #blockuser').hide();	
				$('#list-user #'+idUser+' #UserBlock').append(
				'<img class="btnBlocks" id="unblockuser" src="img/unblock.png" onClick="UnBlockUser('+idUser+')"></img>');	
				}
		});

	};
	
	//define unblock user
	
	UnBlockUser=function(idUser){
	//unblock user in database
		$.get('Admin/UnBlockUser',{
			idUser:idUser
		},
		function(result){
				if(result.isUnBlock==true){
					$('#list-user #'+idUser+' #UserBlock #unblockuser').hide();	
					$('#list-user #'+idUser+' #UserBlock').append(
					'<img class="btnBlocks" id="blockuser" src="img/block.png" onClick="BlockUser('+idUser+')"></img>');
				}
		});
	
	};
	
	//show all songs when load page
		showSongs();

	//define manage song when click on menu
		ManageSong=function(){
			$('#UserManagePage').hide();
			$('#AutoPlaySongPage').hide();
			$('#ControlPage').hide();
			$('#ControlPage').removeClass('active');
			$('#ManageUser').removeClass('active');
			$('#AutoplaySong').removeClass('active');
			$('#ManageSong').addClass('active');
			$('#SongManagePage').show();
			showSongs();
		};
	
	//define manage user when click on menu
		ManageUser=function(){
			$('#SongManagePage').hide();
			$('#AutoPlaySongPage').hide();
			$('#ControlPage').hide();
			$('#ManageSong').removeClass('active');
			$('#AutoplaySong').removeClass('active');
			$('#ControlPage').removeClass('active');
			$('#ManageUser').addClass('active');
			$('#UserManagePage').show();
			showUsers();
		};
	
	//define manage autoplay song when click on menu
		AutoplaySong=function(){
			$('#UserManagePage').hide();
			$('#SongManagePage').hide();
			$('#ControlPage').hide();
			$('#ManageUser').removeClass('active');
			$('#ManageSong').removeClass('active');
			$('#ControlPage').removeClass('active');
			$('#AutoplaySong').addClass('active');
			$('#AutoPlaySongPage').show();
			showListAutoplay();
		};
	
	//define master/slave song when click on menu
		Control=function(){
			$('#UserManagePage').hide();
			$('#SongManagePage').hide();
			$('#AutoPlaySongPage').hide();
			$('#ManageUser').removeClass('active');
			$('#ManageSong').removeClass('active');
			$('#AutoplaySong').removeClass('active');
			$('#ControlPage').addClass('active');
			$('#ControlPage').show();
		};

	//realtime for add a Song
	socket.on('addSongToPlaylistClient',function (data){
		console.log('Da add vao trang admin'+data.idSong+data.titleSong);
		var title='<li id="'+data.idSong+'" class="list-group-item list-group-item-success">'+data.titleSong,
			priority='<label class="label label-danger pull-left" id="priority">-</label>',
			block='<span class="block" id="SongBlock">'+
				'<img class="btnBlocks" id="blocksong" src="img/block.png" onClick="BlockSong('+data.idSong+')"></img>'+
				'</span>'+	
				'</li>',
			unblock='<span class="block" id="SongBlock">'+
				'<img class="btnBlocks" id="unblocksong" src="img/unblock.png" onClick="UnBlockSong('+data.idSong+')"></img>'+
				'</span>'+	
				'</li>';
			if(data.isBlock==false){
					$('#listsong').append(title+priority+block);
			}
			else{
				$('#listsong').append(title+priority+unblock);
			}
	});	
	socket.on('deleteSong',function(message){
		var idSong=message[0];
		$('#listsong #'+idSong).remove();
	});
});