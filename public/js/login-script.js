var loginForm = $("#btn-submit").closest('#login-form'),
		emailInput = loginForm.find('#input-email'),
		passwordInput = loginForm.find('#input-password'),
		confirmPassInput = loginForm.find('#confirm-password-input'),
		cbxRemember = loginForm.find('#cbx-remember');
$(document).ready(function(){
	// check valid email
	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	
	// check valid password
	function isPassword(pass) {
		// at least one number, one lowercase and one uppercase letter
		// at least six characters that are letters, numbers or the underscore
		//var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
		var regex = /^\S{6,}$/;
		return regex.test(pass);
	}
	
	//switch between login form and register form	
	$('#login-form').on('click','#btn-switch', function(){		
		passwordInput.val("");
		confirmPassInput.val("");
		emailInput.popover("hide");
		passwordInput.popover("hide");
		confirmPassInput.popover("hide");
		$('#error_messages').html("");
		if($(this).text()==="Don't Have an Account?") {
			$(this).text("Login!");
			loginForm.find('#checkbox-form').hide();
			loginForm.find('#confirm-password-input').fadeIn(1000);
			loginForm.find('#btn-submit').text("Register");
			//Nguyen Van Quang Tan
			//Change the from action to register
			document.getElementById("login-form").action = "register";

		} else {
			$(this).text("Don't Have an Account?");
			loginForm.find('#checkbox-form').fadeIn(1000);
			loginForm.find('#confirm-password-input').hide();
			loginForm.find('#btn-submit').text("Enjoy");
			//Nguyen Van Quang Tan
			//Change the from action to login
			document.getElementById("login-form").action = "login";
		}
	});
	
	//login function
	var loginEvent = function(){
		var emailVal = emailInput.val(),
		passVal = passwordInput.val(),
		confirmPassVal = confirmPassInput.val();		
		if (!isEmail(emailVal)){
			emailInput.popover("show");
		} else {
			emailInput.popover("hide");
		}
		if (!isPassword(passVal)){
			passwordInput.popover("show");
		} else {
			passwordInput.popover("hide");
		}
		if (confirmPassVal!=passVal){
			confirmPassInput.popover("show");
		} else {
			confirmPassInput.popover("hide");
		}
		//submit data
		if ($("#btn-submit").text()==="Enjoy"
			&& isEmail(emailVal)
			&& isPassword(passVal)){
			if (cbxRemember.is(':checked')) {
				setCookie("username",emailVal,365);
				setCookie("pass",passVal,365);
			} else {
				setCookie("username","",365);
				setCookie("pass","",365);
			}
			loginForm.submit();
		} else if ($(this).text()==="Register"
			&& isEmail(emailVal)
			&& isPassword(passVal)
			&& confirmPassVal===passVal) {
			var _token = $('input[name="_token"]').val();
			$.post('checkemail',{ _token:_token, email: $('#input-email').val()},function(result){
					if (result == 'true')
						loginForm.submit();
					else{
						$('#error_messages').html(result);
						$('#error_messages').show();
					}
				},'text');
		}
	};
	
	// set listener
	loginForm.on('click','#btn-submit',loginEvent);
	loginForm.on('keypress','#input-email',function(e){
		if(e.keyCode==13){			
			e.preventDefault();
			loginEvent();
		}
	});
	loginForm.on('keypress','#input-password',function(e){
		if(e.keyCode==13){			
			e.preventDefault();
			loginEvent();
		}
	});
	loginForm.on('keypress','#confirm-password-input',function(e){
		if(e.keyCode==13){			
			e.preventDefault();
			loginEvent();
		}
	});
});