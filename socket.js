var app =require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket)
{
	console.log('Had new connection connected');

	socket.on('chat-channelt',function (data){
		io.emit('testchannel',data);
	});

	socket.on('addSong',function (data){
		io.emit('addSongClient', data);
	});
});
server.listen(3000);