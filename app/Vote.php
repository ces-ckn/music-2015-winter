<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DateTime;

class Vote extends Model
{
    protected $table = 'vote';
    //protected $maxVotePerDate=5;
    public static function addVote($idAccount, $idSong)
    {    	
    	$date=date_format(new DateTime('NOW'),'Y-m-d');
    	$vote=new Vote();
    	$vote->idAccount=$idAccount;
    	$vote->date=$date;
    	$vote->idSong=$idSong;
    	$vote->save();
    }

    public static function countVoteByAccount($idAccount)
    {
    	$date=date_format(new DateTime('NOW'),'Y-m-d');
    	$count=Vote::where('date','=',$date)->where('idAccount',$idAccount)->count();
    	return $count;
    }

    public static function isAccountSongNOTExists($idAccount,$idSong)
    {
    	$date=date_format(new DateTime('NOW'),'Y-m-d');
    	$count=Vote::where('date','=',$date)->where('idAccount',$idAccount)->where('idSong',$idSong)->count();
    	if($count>0)
    		return false;
    	else
    		return true;
    }

    public static function getIdSongsVoted($idAccount){
        $date=date_format(new DateTime('NOW'),'Y-m-d');
        $idSongs=Vote::where('idAccount',$idAccount)->where('date','=',$date)->select('idSong')->get();
        $list=array();
        foreach ($idSongs as $idsong) {
            array_push($list,$idsong->idSong);
        }
        return $list;
    }

    //get all vote for report
    public static function getAllVote()
    {
    	$listVote=Vote::get();
    	return $listVote;
    }
}
