<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Filter
Route::filter("check-login",function(){
	if (!Session::has("logined"))
		return Redirect::to("login");
});

Route::filter("check-admin",function(){
	if(!Session::has("logined"))
		return Redirect::to("login");
	else{
		if(!session('logined')[2])
			return Redirect::to("login");
	}
});

//Login
Route::get('/',array("before"=>"check-login",function(){
	return Redirect::to('home');
}));

Route::get('login','LoginController@login');
Route::post('login','LoginController@doLogin');

//Change password
Route::post('checkpass','ChangePasswordController@checkPassword');
Route::post('changepass','ChangePasswordController@changePassword');

//Register
Route::post('register','RegisterController@register');
Route::post('checkemail','RegisterController@checkEmailExists');

//Logout
Route::get('logout',function(){
	Session::forget('logined');
	return Redirect::to("login");
});

//Home
Route::get('home',array("before"=>"check-login",function(){
	return view("home");
}));

Route::get('home',array("before"=>"check-login",'uses'=>'HomeController@getHome'));

Route::get('addSong',array("before"=>"check-login",'uses'=>'HomeController@addSong'));

Route::get('getAllIdSong',array("before"=>"check-login",'uses'=>'HomeController@getAllIdSong'));

Route::get('home/deleteASong',array("before"=>"check-login",'uses'=>'HomeController@deleteSong'));

//Home -> add vote
Route::get('home/voteUp',array("before"=>"check-login",'uses'=>'HomeController@voteUp'));

Route::get('home/voteDown',array("before"=>"check-login",'uses'=>'HomeController@voteDown'));

Route::get('home/getCountdown',array("before"=>"check-login",'uses'=>'HomeController@getCountdown'));

//test
Route::get('count','HomeController@countby');

//admin
//Route::get('admin',function(){
//	return view('admin');
//});
Route::get('admin',array("before"=>"check-admin",'uses'=>'AdminController@getAdminPage'));

Route::get('getSongByID',array("before"=>"check-login",'uses'=>'HomeController@getSongByID'));

//admin->get songs
Route::get('getAllSongAdmin',array("before"=>"check-admin",'uses'=>'AdminController@getAllSongAdmin'));

//admin->get users'
Route::get('getAllUserAdmin',array("before"=>"check-admin",'uses'=>'AdminController@getAllUserAdmin'));

//admin->blocksong
Route::get('Admin/BlockSong',array("before"=>"check-admin",'uses'=>'AdminController@BlockSong'));

//admin->unblocksong

Route::get('Admin/UnBlockSong',array("before"=>"check-admin",'uses'=>'AdminController@UnBlockSong'));

//admin->blockuser
Route::get('Admin/BlockUser',array("before"=>"check-admin",'uses'=>'AdminController@BlockUser'));

//admin->unblockuser
Route::get('Admin/UnBlockUser',array("before"=>"check-admin",'uses'=>'AdminController@UnBlockUser'));

//admin->getlist autoplay
Route::get('GetAutoList',array("before"=>"check-admin",'uses'=>'AdminController@GetAutoList'));

//admin->addAutoplayPoint
Route::get('Admin/addAutoplayPoint',array("before"=>"check-admin",'uses'=>'AdminController@addAutoPlayPoint'));

//admin->DeleteAutoplay
Route::get('Admin/DeleteAutoplayPoint',array("before"=>"check-admin",'uses'=>'AdminController@deleteAutoPlayPoint'));

Route::get('getListIdSongVoted',array("before"=>"check-login",'uses'=>'HomeController@getListIdSongVoted'));