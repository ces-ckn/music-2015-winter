<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use App\Account;
use Redirect;
use Validator;

class RegisterController extends Controller
{
    public function register(Request $request){
    	//validate input data
    	$validator = Validator::make($request->all(),[
    		'email'=>'required|email',
    		'password'=>'required|min:6',
    		'confirmpassword'=>'required|min:6']);

    	//check validator
    	if ($validator->fails()){
    		return redirect('login');
    	}else{
    		if ($request['password']===$request['confirmpassword']){
    			//password is encoded by md5
    			if (!Account::is_Account_exists($request['email'])){
    				Account::register_Account($request['email'],md5(sha1($request['password'])));
    				Session::put("logined",[$request['email'],Account::getIdAccount($request['email']),false]);
    				return redirect('home');
    			}else {
    				return redirect('login')->with("registorError","The email is already being used");
    			}
    		}else return redirect('login');
    	}
    }

    public function checkEmailExists(){
    	if (Account::is_Account_exists($_POST['email'])){
    		return "The email is already being used";
    	}else
    		return "true";
    }
}
