<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Song;
use App\Account;
use App\Auto;
use App\Vote;
use \DateTime;
use Illuminate\Support\Facades\Session;
//broadcast
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

class HomeController extends Controller
{

	//private $client = new Client(new Version1X('https://nodemusic.herokuapp.com'));
	
	public function getListIdSongVoted(){
		$idAccount=session('logined')[1];
		return Vote::getIdSongsVoted($idAccount);
	}

	public function getHome(){
		$idAccount=session('logined')[1];
		$list= Song::getSongs();
		return view('home')->with('songs',$list)
							->with('vote',5-Vote::countVoteByAccount($idAccount))
							->with('idSongsVoted',Vote::getIdSongsVoted($idAccount));
	}
	public function addSong(){
		$song = $_GET['clickedTrack'];
		$song = json_decode($_GET['clickedTrack'],true);
		$title = $song['titleSong'];
		$idAccount = session('logined')[1];
		$priority = Song::getMaxPriority()+1;
		if(Song::addSong($song['idSong'],$song['titleSong'],$song['artist'],$song['linkSong'],$idAccount,$song['duration'],$priority,$song['imageLink'])){
			
			$client = new Client(new Version1X('localhost:5000'));
    		$client->initialize();
			$client->emit('addSongToPlaylist', ['idSong' => $song['idSong'], 'titleSong'=> $song['titleSong'], 'priority' => $priority,'isBlock'=>false,'duration'=>$song['duration'], 'idAccount'=>$idAccount]);
        	$client->close();
        	return 'Add '+$song['idSong']+' success';
		}        
		return 'Add '+$song['idSong']+' failed';
	}

	public function getAllIdSong(){
		//$ids = Song::getIdSongs();
		$ids=array();
		foreach(Song::getIdSongs() as $id){
			array_push($ids,$id->idSong);
		}

		$idSongsOfAccount=array();
		foreach(Song::getAllIdSongOfAccount(session('logined')[1]) as $id){
			array_push($idSongsOfAccount,$id->idSong);
		}

		return ['IdSongs'=>$ids,'IdSongsOfAccount'=>$idSongsOfAccount];
	}

	public function deleteSong(){
		$idSong= (int)$_GET['idSong'];
		$priority= Song::getPriorityById($idSong);
		if ($priority!=null){
			if ($priority>0){
				Song::updatePriorityOfSongs($priority);
				Song::deleteSongByIdSong($idSong);
				return $priority;
			}
		}
	}

	//vote Up
	public function voteUp()
	{
		$isVoteUp=true;
		$idSong=(int)$_GET['IDSong'];
		$priority=(int)$_GET['Priority'];
		$idAccount=(int)$_GET['IDAccount'];
		//return view('test')->with('song',$idSong)->with('account',$idAccount)->with('priority',$priority);
		//if account can vote
		$countVoteByAccount=Vote::countVoteByAccount($idAccount);
		$checkVote=Vote::isAccountSongNOTExists($idAccount,$idSong);
		if($countVoteByAccount<5 && $checkVote && $priority>1)
		{
			Vote::addVote($idAccount, $idSong);
			$song = Song::updatePriorityForVote($priority,$isVoteUp);
			if ($song->priority == 1){
				$client = new Client(new Version1X('localhost:5000'));
    			$client->initialize();
				$client->emit('nextSong',[$song->idSong,$song->time]);
        		$client->close();
			}
			$countVoteByAccount++;
		}else{
			$checkVote=false;
		}
		return array($countVoteByAccount, $checkVote);
		
	}

	//vote Down
	public function voteDown()
	{
		$isVoteUp=false;
		$idSong=(int)$_GET['IDSong'];
		$priority=(int)$_GET['Priority'];
		$idAccount=(int)$_GET['IDAccount'];
		//return view('test')->with('song',$idSong)->with('account',$idAccount)->with('priority',$priority);
		$countVoteByAccount=Vote::countVoteByAccount($idAccount);
		$checkVote=Vote::isAccountSongNOTExists($idAccount,$idSong);
		$maxPriority=Song::getMaxPriority();
		if($countVoteByAccount<5 && $checkVote && $priority<$maxPriority)
		{
			Vote::addVote($idAccount, $idSong);
			$song = Song::updatePriorityForVote($priority,$isVoteUp);
			if ($song->priority == 1){
				$client = new Client(new Version1X('localhost:5000'));
    			$client->initialize();
				$client->emit('nextSong',[$song->idSong,$song->time]);
        		$client->close();
			}
			$countVoteByAccount++;
		}else{
			$checkVote=false;
		}
		return array($countVoteByAccount, $checkVote);
	}


	//count vote by account
	public function countVoteByAccount()
	{
		//code get idAccount here
		//...
		//code
		$date=date_format(new DateTime('NOW'),'Y-m-d');
		return Vote::countVoteByAccount($idAccount,$date);
	}

	public function countby(){
		$value='false';
		if(Vote::isAccountSongNOTExists(1,150494156))
			$value='true';
		return view('test')->with('value',$value);
	}

	public function getSongByID(){
		$idSong=(int)$_GET['IDSong'];
		$song = Song::getSongByID($idSong);
		if($song)
		{
			return ['idSong'=>$song->idSong, 'titleSong'=>$song->title, 'priority'=>$song->priority, 'duration'=>$song->time];
		}
		//return ['priority'=>$priority,'titleSong'=>$title];
		return null;
	}

	//getCountdown
	public function getCountdown(){
		$time=Auto::getCountdown();
		return $time;
	}
}
