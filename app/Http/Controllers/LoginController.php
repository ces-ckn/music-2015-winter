<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\modelLogin;
use App\Account;
use Redirect;

class LoginController extends Controller{
	public function login(){
		return view('login');
	}


	public function doLogin(){
		$username = $_POST['email'];
 		$password = $_POST['password'];
 		if (Account::check_login($username,md5(sha1($password)))){
 			// put username and idAccount in session
 			Session::put("logined",[$username,Account::getIdAccount($username),Account::isAdmin($username)]);
			return redirect("home");
 		}
 		else return view('login')->with("error","Username or password is invalid!");
	}
}