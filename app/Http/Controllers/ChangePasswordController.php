<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\modelLogin;
use App\Account;
use Redirect;

class ChangePasswordController extends Controller{
	public function checkPassword(){
		$idAccount = session('logined')[1];
		$pass = $_POST['pass'];
		if(Account::checkPassword($idAccount,md5(sha1($pass)))){
			return "true";
		}else{
			return "false";
		}
	}
	
	public function changePassword(){
		$newpass = $_POST['newPass'];
		$idAccount = session('logined')[1];
		Account::updateNewPassword($idAccount,md5(sha1($newpass)));
		return "Your password has been changed!";
 	}
}