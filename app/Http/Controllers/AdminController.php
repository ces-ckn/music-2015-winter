<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Song;
use App\Account;
use App\Auto;
use App\Vote;
use \DateTime;
use Illuminate\Support\Facades\Session;
//broadcast
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

class AdminController extends Controller
{
	public function getAdminPage(){
		return view('admin');
	}

	public function getAllSongAdmin(){
		//$songs = Song::getSongs();
		//$songs=array();
		$songs=Song::getSongsAdmin();
		return $songs;
	}
	public function getAllUserAdmin(){
		$users=Account::getAllUserAdmin();
		return $users;
	}
	
	public function GetAutoList(){
		$list=Auto::GetListAuto();
		return $list;
	}

	//block/unblock song
	public function BlockSong(){
		$idSong= (int)$_GET['IDSong'];
		$priority=Song::getPriorityById($idSong);
		if(Song::BlockSong($idSong)){

			//emit to nodejs server
			$client = new Client(new Version1X('localhost:5000'));
    		$client->initialize();
			$client->emit('blockSong', ['idSong' => $idSong,'priority'=>$priority]);
        	$client->close();
			return ['IDSong'=>$idSong,'isBlock'=> true];
		}
		else{
			return ['IDSong'=>$idSong, 'isBlock' =>false];
		}
	}
	public function UnBlockSong(){
		$idSong= (int)$_GET['IDSong'];
		if(Song::UnBlockSong($idSong)){
			return ['IDSong'=>$idSong,'isUnBlock'=> true];
		}
		else{
			return ['IDSong'=>$idSong, 'isUnBlock' =>false];
		}
	}

	//Block/Unblock User
	public function BlockUser(){
		$idUser= (int)$_GET['idUser'];
		if(Account::BlockUser($idUser)){
			return ['idUser'=>$idUser,'isBlock'=> true];
		}
		else{
			return ['idUser'=>$idUser, 'isBlock' =>false];
		}
	}
	public function UnBlockUser(){
		$idUser= (int)$_GET['idUser'];
		if(Account::UnBlockUser($idUser)){
			return ['IDUser'=>$idUser,'isUnBlock'=> true];
		}
		else{
			return ['IDUser'=>$idUser, 'isUnBlock' =>false];
		}
	}

	//Auto Play time
	public function addAutoPlayPoint(){
		//date_default_timezone_set("Asia/Ho_Chi_Minh");
		$timeStart=strtotime($_GET['timeStart']);		
       //$curent=date('Y-m-d H:i:s');
		//$diff=$timeStart-strtotime($curent);
		$duration=(int)$_GET['duration'];
		$number=(int)$_GET['number'];
		$startTime=date('Y-m-d H:i:s',$timeStart);
		//return $startTime;
		$auto=Auto::addAutoPlayPoint($startTime,$duration,$number);
		if($auto){
			return ['idAuto'=>$auto->id];
		}
		else{
			return null;
		}
		
	}

	public function deleteAutoPlayPoint(){
		$idAuto=(int)$_GET['idAuto'];
		if(Auto::deleteAuto($idAuto)){
			return $idAuto;
		}
		return null;
		
	}
}
