<?php

namespace App;

use \DateTime;
use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    protected $table = 'auto';
	public static function GetListAuto(){
		$list=Auto::orderby('startTime')->get();
		return $list;
	}
	public static function addAutoPlayPoint($timeStart,$duration,$number){
    	$auto=new Auto();
    	$auto->startTime=$timeStart;
    	$auto->duration=$duration;
    	$auto->number=$number;
    	$auto->save();
    	return $auto;
    }
    public static function deleteAuto($idAuto){
    	$auto=Auto::where('idAuto',$idAuto)->first();
    	//return $auto;
    	if($auto){
    		Auto::where('idAuto',$idAuto)->delete();
    		return true;
    	}
    	else 
    		return false;    	
    }


    public static function getCountdown(){       
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $date=date('Y-m-d H:i:s');
        if(Auto::where('startTime','>=',$date)->orderby('startTime')->first()){
            $startTime=Auto::where('startTime','>=',$date)->orderby('startTime')->first()->startTime;

            $diff=strtotime($startTime)-strtotime($date);
            if($diff>0 && $diff<=86400){    
                return $diff;    
            }         
            else {
                return $diff;
            }
        }
        else{
            return null;
        }        
    }
}
