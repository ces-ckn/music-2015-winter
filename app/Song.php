<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $table = 'song';
    protected $primaryKey='idSong';

    public static function addSong($idSong,$title,$artist,$link,$idAccount,$time,$priority,$image){
        $song=Song::find($idSong);
        if ($song){
            if($song->isBlock){
                return false;
            }
            else{
                $song->idAccount=$idAccount;
                $song->priority=$priority;
                $song->save();
                return true;
            }
            
        }
    	$song = new Song();
    	$song->idSong=$idSong;
    	$song->title=$title;
    	$song->artist=$artist;
    	$song->link=$link;
    	$song->idAccount=$idAccount;
    	$song->time=$time;
    	$song->priority=$priority;
    	$song->image=$image;
    	$song->save();
        return true;
    }

    public static function getMaxPriority(){
    	$gtri=0;
    	$max= Song::max('priority');
        if($max){
            if($max<0)
                $max=0;
        }
    	//return !$max?0:$max[0].priority;
        return !$max?0:$max;
        /*if (!$max)
            return 0;
        return $max['priority'];*/
    }

    public static function getSongs(){
        $list = Song::where('priority','>',0)->where('isBlock',false)->orderby('priority')->get();
        //$list=Song::get();
        return $list;
    }

    public static function getSongsAdmin(){
        $list = Song::where('priority','>',0)->orWhere('isBlock',true)->orderby('priority')->get();
        //$list=Song::get();
        return $list;
    }

    public static function getIdSongs(){
        $ids = Song::where('priority','>',0)->select('idSong')->get();
        return $ids;
    }

    public static function getAllIdSongOfAccount($idAccount){
        $ids = Song::where('idAccount',$idAccount)->where('priority','>',0)->get();
        return $ids;
    }

    public static function getPriorityById($idSong){
       // $priority= Song::where('idSong',$idSong)->select('priority')->get();
       // return $priority[0]->priority;
        $song=Song::where('idSong',$idSong)->first();
        if($song){
            return $song->priority;
        }
        return null;
    }

    public static function updatePriorityOfSongs($priority){
        $songs= Song::where('priority','>',$priority)->get();
        foreach ($songs as $song){
            $song->priority= $song->priority -1;
            $song->save();
        }
    }

    public static function deleteSongByIdSong($idSong){
        $song= Song::find($idSong);
        if ($song){
            $song->priority=-1;
            $song->save();
        }

    }

    //update priority for vote
    public static function updatePriorityForVote($currentPriority,$isVoteUp)
    {
        if($isVoteUp)
        {
            if($currentPriority==1)
            {
                //if The song is hightest priority, can't vote up
                return null;
            }
            else
            {
                //select
                $song=Song::where('priority','=',($currentPriority-1))->first();
                $currentSong=Song::where('priority','=',$currentPriority)->first();

                //decreas the priority of song infront of current song.
                //increas the priority of current song
                $song->priority=$song->priority+1;
                $currentSong->priority=$currentSong->priority-1;

                //update database
                $song->update();
                $currentSong->update();
                if ($song->priority < $currentSong->priority)
                    return $song;
                else 
                    return $currentSong;
            }           
        }
        else 
        {
            if($currentPriority==Song::getMaxPriority())
            {
                //if the song is lowest priority, can't vote down
                return null;
            }
            else
            {
                //select
                $song=Song::where('priority','=',($currentPriority+1))->first();
                $currentSong=Song::where('priority','=',$currentPriority)->first();
          
                //increas the priority of song infront of current song.
                //decreas the priority of current song                
                $song->priority=$song->priority-1;
                $currentSong->priority=$currentSong->priority+1;

                //update database
                $song->update();
                $currentSong->update();
                if ($song->priority < $currentSong->priority)
                    return $song;
                else 
                    return $currentSong;
            }
        }
    }

    public static function getSongByID($idSong){
        $song = Song::where('idSong',$idSong)->first();
        return $song;
    }

    //block song
    public static function BlockSong($idSong)  
    {
        $song =Song::where('idSong',$idSong)->where('priority','>',0)->first();
        if($song)
        {
            $song->isBlock=true;
            $priority=$song->priority;
            $song->priority=0;
            $song->update();
            Song::updatePriorityOfSongs($priority);
            return true;
        }
        else {
            return false;
        }
    }

    //unblock song
    public static function UnBlockSong($idSong)
    {
        $song =Song::where('idSong',$idSong)->first();
        if($song)
        {
            $song->isBlock=false;
            $song->priority=0;
            $song->update();
            return true;
        }
        else {
            return false;
        }
    }
}
